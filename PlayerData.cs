﻿// PlayerData.cs
using System;
using System.Xml;
using System.Xml.Serialization;

/// <summary>
/// A class that contains Xml fields with data from players
/// </summary>
/// <remarks>
/// 
/// </remarks>
public class PlayerData
{
    /* To Do:
        String checking
            is not null
            is not too long
            contains normal characters
            contains no swear words or other offensive materials
        Score checking
            If score is 0, dont write
     */
    [XmlAttribute("name")] public string Name;
    [XmlAttribute("score")] public int Score;
    [XmlAttribute("date")] public DateTime Date;
    /*
        Optional:
            Amount of rings hit
            Amount of (types of) buildings hit
    */
}
