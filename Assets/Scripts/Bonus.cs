﻿ // Bonus.cs
using UnityEngine;

//
// This class controls the given bonus when completing a level.
// The bonus is higher the longer you play.
public class Bonus : MonoBehaviour
{

    [SerializeField] private float m_bonusValue;            // The value of the bonus points
    [SerializeField] private float m_bonusStartTime = 60f;  // The bonus drop starting time    
    private UnityEngine.UI.Text m_text;                     // The text component which displays the current bonus value 
    private bool m_started = false;                         // Boolean to check of the game has started
    private GameMaster m_master;                            // Reference to the GameMaster object

    //
    // Returns the bonusvalue.
    public float BonusValue { get { return m_bonusValue; } }
    
    //
    // Initializes this instance.
    private void Awake()
    {
        m_text = GetComponent<UnityEngine.UI.Text>();       // Set the reference to the Text component
        m_master = GameObject.Find("GameMaster").GetComponent<GameMaster>();
    }
    
    //
    // Start is called on this instance after the Awake function.
    void Start ()
    {
        m_bonusValue = m_master.GameTimeMode * 10000;       // The value of the bonus is 30000 for 3 minutes, 50000 for 5 minutes and 80000 for 8 minutes
        m_bonusStartTime = 60f;                             // The bonus starts decreasing after 1 minute into starting the game
        m_text.text = Mathf.Round(m_bonusValue).ToString();
    }
    


    // 
    // The game loop for this instance.
    void Update ()
    {
        m_text.text = Mathf.RoundToInt(m_bonusValue).ToString();
    }

    internal System.Collections.IEnumerator DecreaseBonus ()
    {
        yield return new WaitForSeconds(m_bonusStartTime);
        while (m_bonusValue > 0)
        {
            m_bonusValue -= 2f;
            yield return new WaitForFixedUpdate();          // Fixed timestep ensures a consistently decreasing bonus amount
        }
        m_bonusValue = 0;
    }
}
