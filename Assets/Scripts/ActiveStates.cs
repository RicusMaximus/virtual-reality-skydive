﻿// ActiveStates.cs

// This class holds a list of the currently active state of the Player.
[UnityEngine.AddComponentMenu("Utility/Active States")]
public class ActiveStates : UnityEngine.MonoBehaviour
{
    public System.Collections.Generic.List<IPlayerState> list = new System.Collections.Generic.List<IPlayerState>(); // The list of Player states 
}