﻿using UnityEngine;
using System.Collections;


public class Parachute : MonoBehaviour
{
    private LineRenderer[] _lines = new LineRenderer[2];
    [SerializeField] private Transform[] _handles = new Transform[2];
    [SerializeField] private Transform[] _handlePivots = new Transform[2];
    [SerializeField] private Transform[] _hands = new Transform[2];
    public bool TogglePara  = false;

    #region Getters & Setters
    public LineRenderer[] Lines
    {
        get
        {
            return _lines;
        }

        set
        {
            _lines = value;
        }
    }

    public Transform[] Handles
    {
        get
        {
            return _handles;
        }

        set
        {
            _handles = value;
        }
    }

    public Transform[] Hands
    {
        get
        {
            return _hands;
        }

        set
        {
            _hands = value;
        }
    }

    public Transform[] HandlePivots
    {
        get
        {
            return _handlePivots;
        }

        set
        {
            _handlePivots = value;
        }
    }
    #endregion
        
    private void Start ()
    {
        Initialize();
    }

    private void Update()
    {
        RenderLines();
        if (TogglePara )
            CalculateHandlePositions();
    }

    private void CalculateHandlePositions()
    {
        Handles[0].transform.position = Hands[0].transform.position;
        Handles[1].transform.position = Hands[1].transform.position;
    }
        
    private void Initialize ()
    {
        Lines[0] = gameObject.transform.Find("LineRendererL").GetComponent<LineRenderer>();
        Lines[1] = gameObject.transform.Find("LineRendererR").GetComponent<LineRenderer>();

        Handles[0] = gameObject.transform.Find("L_Handle").GetComponent<Transform>();
        Handles[1] = gameObject.transform.Find("R_Handle").GetComponent<Transform>();

        HandlePivots[0] = Handles[0].Find("LeftHandlePivot").GetComponent<Transform>();
        HandlePivots[1] = Handles[1].Find("RightHandlePivot").GetComponent<Transform>();

        Hands[0] = gameObject.transform.parent.Find("Vanguard/mixamorig:Hips/mixamorig:Spine/mixamorig:Spine1/mixamorig:Spine2/mixamorig:LeftShoulder/mixamorig:LeftArm/mixamorig:LeftForeArm/mixamorig:LeftHand/LeftHandPalm").GetComponent<Transform>();
        Hands[1] = gameObject.transform.parent.Find("Vanguard/mixamorig:Hips/mixamorig:Spine/mixamorig:Spine1/mixamorig:Spine2/mixamorig:RightShoulder/mixamorig:RightArm/mixamorig:RightForeArm/mixamorig:RightHand/RightHandPalm").GetComponent<Transform>();
    }

    private void RenderLines()
    {
        Lines[0].SetPosition(0, Handles[0].localPosition + HandlePivots[0].localPosition);
        Lines[1].SetPosition(0, Handles[1].localPosition + HandlePivots[1].localPosition);
    }
}