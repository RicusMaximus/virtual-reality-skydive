﻿// ParachuteBehaviour.cs
using UnityEngine;
using System.Collections;


/// <summary>
/// Class level summary.
/// </summary>
/// <remarks>
/// Longer comments associated with a type or member
/// </remarks>
public class ParachuteBehaviour : StateMachineBehaviour, IPlayerState
{

    /// <summary>
    /// The hit information of the raycast is stored in this.
    /// </summary>
    private RaycastHit hit;
    /// <summary>
    /// The Controller script reference of the player.
    /// </summary>
    private PlayerController m_playerControl;
    /// <summary>
    /// The Animator attached to this Animator Controller.
    /// </summary>
    private Animator m_anim;
    /// <summary>
    /// The forces vector.
    /// </summary>
    /// <remarks>
    /// The vector in which all external forces are accumulated.
    /// </remarks>
    private Vector3 m_forces;
    /// <summary>
    /// The drag factor
    /// </summary>
    private float m_dragFactor = 0.005f;
    /// <summary>
    /// The brake factor
    /// </summary>
    private float m_brakeFactor;
    /// <summary>
    /// The lift force factor
    /// </summary>
    private float m_liftForceFactor = 8f;
    /// <summary>
    /// The forward direction
    /// </summary>
    /// <remarks>
    /// The forward direction is the local forward direction projected on the world x,y plane.
    /// </remarks>
    private Vector3 m_flatForward;

    /// <summary>
    /// The thrust factor
    /// </summary>
    private float m_thrustFactor = 10f;
    /// <summary>
    /// The yaw rotation speed factor.
    /// </summary>
    private float m_yawFactor = 0.5f;
    /// <summary>
    /// The current pitch angle.
    /// </summary>
    private float m_pitchAngle;
    /// <summary>
    /// The current roll angle.
    /// </summary>
    private float m_rollAngle;
    /// <summary>
    /// The parachute drag factor.
    /// </summary>
    private float m_parachuteDrag = 1f;

    private IEnumerator m_warning;

    public float ForwardSpeed { get; private set; }                 // How fast the player is traveling in his forward direction.
    
    /// <summary>
    /// OnStateEnter is called before OnStateEnter is called on any state inside this state machine.
    /// </summary>
    /// <param name="animator">The attached Animator.</param>
    /// <param name="stateInfo">The current Animator State information.</param>
    /// <param name="layerIndex">Index of the layer.</param>
    override public void OnStateEnter(Animator animator, AnimatorStateInfo stateInfo, int layerIndex) {

		// Assign private variables
		m_playerControl = animator.transform.gameObject.GetComponent<PlayerController>();
		m_anim = animator;
		m_playerControl.OpenParachute();

        MonoBehaviour.print("Delay Parachute!");
        m_playerControl.FfToggled = true;
        m_playerControl.StartCoroutine(m_playerControl.DelayFreefall());
        m_playerControl.StartCoroutine(m_playerControl.DelayParachute());

        // Add State to active state list
        ActiveStates activeStates = animator.GetComponent<ActiveStates> ();
        activeStates.list.Add(this);
	}


    /// <summary>
    /// OnStateUpdate is called before OnStateUpdate is called on any state inside this state machine.
    /// </summary>
    /// <param name="animator">The attached Animator.</param>
    /// <param name="stateInfo">The current Animator State information.</param>
    /// <param name="layerIndex">Index of the layer.</param>
    override public void OnStateUpdate(Animator animator, AnimatorStateInfo stateInfo, int layerIndex) {
		EvaluateState ();
		Behaviour ();
	}



    /// <summary>
    /// OnStateExit is called before OnStateExit is called on any state inside this state machine.
    /// </summary>
    /// <param name="animator">The attached Animator.</param>
    /// <param name="stateInfo">The current Animator State information.</param>
    /// <param name="layerIndex">Index of the layer.</param>
    override public void OnStateExit(Animator animator, AnimatorStateInfo stateInfo, int layerIndex) {

		// Remove State from active state list and call call OnTransitionComplete
		ActiveStates activeStates = animator.GetComponent<ActiveStates> ();
		activeStates.list.Remove (this);
		if (activeStates.list.Count > 0)
		{
			activeStates.list[0].OnTransitionComplete(animator, stateInfo, layerIndex);
		}

		// Remove Parachute clone
		m_playerControl.DropParachute();
	}

    /// <summary>
    /// Called on the first Update frame when a transition from a state from another statemachine transition to one of this statemachine's state.
    /// </summary>
    /// <param name="animator">The attached Animator.</param>
    /// <param name="stateMachinePathHash">The full path hash for this state machine.</param>
    override public void OnStateMachineEnter(Animator animator, int stateMachinePathHash){
		animator.applyRootMotion = false;
	}

    /// <summary>
    /// Called when the animation transition is completed.
    /// </summary>
    /// <param name="animator">The attached Animator.</param>
    /// <param name="stateInfo">The current Animator State information.</param>
    /// <param name="layerIndex">Index of the layer.</param>
    public void OnTransitionComplete(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
	{
        MonoBehaviour.print("Reset Trigger OpenParachute!");
        m_anim.ResetTrigger("OpenParachute");
        // Give the player a warning when the parachuting is taking up too much time
        m_warning = m_playerControl.WarnPlayer(); // Set Enumerator reference to WarnPlayer function
        m_playerControl.StartCoroutine(m_warning); // Call the WarnPlayer function as a coroutine

        
    }
        
    /// <summary>
    /// Evaluates the state and changes internal states.
    /// </summary>
    private void EvaluateState()
	{
		if (m_playerControl.Altitude < (5.0f * -m_playerControl.Rig.velocity.y * 0.02f)) // Goes to the landing sequence if the player reaches close to the ground, the faster the player falls (e.g. not braking) the sooner it's triggered.
		{
            m_anim.SetTrigger("Landing");
		}
        if (!m_playerControl.FfToggled)
        {
            if (m_playerControl.TogglePara) // The player drops his parachute to go back to freefall.
            {
                MonoBehaviour.print("Set Trigger Freefall!");
                m_anim.SetTrigger("Freefall");
            }
        }
	}


    /// <summary>
    /// Calls the appropriate functions for this behaviour in the right order.
    /// </summary>
    private void Behaviour()
    {
		CalculateAngles();

        CalculateForwardSpeed();

		CalculateBrakeFactor();

		CalculateDrag();

		CalculateForces();
        
		RotatePlayer();

		ControlPlayerAnimationState();
	}

    /// <summary>
    /// Calculates the current rotation angles of the player as a factor.
    /// </summary>
    /// <remarks>
    /// Credits to Unity Standard Assets for the AeroplaneController script where airplane dynamics are calculated.
    /// </remarks>
    private void CalculateAngles() 
	{
		// Calculate roll & pitch angles
		// Calculate the flat forward direction (with no y component).
		m_flatForward = m_playerControl.transform.forward;
		m_flatForward.y = 0;
		// If the flat forward vector is non-zero (which would only happen if the plane was pointing exactly straight upwards)
		if (m_flatForward.sqrMagnitude > 0)
		{
			m_flatForward.Normalize();
			// calculate current pitch angle
			var localFlatForward = m_playerControl.transform.InverseTransformDirection(m_flatForward);
			m_pitchAngle = Mathf.Atan2(localFlatForward.y, localFlatForward.z);
			// calculate current roll angle
			var flatRight = Vector3.Cross(Vector3.up, m_flatForward);
			var localFlatRight = m_playerControl.transform.InverseTransformDirection(flatRight);
			m_rollAngle = Mathf.Atan2(localFlatRight.y, localFlatRight.x);
		}
	}



    /// <summary>
    /// Calculates the brake factor.
    /// </summary>
    /// <remarks>
    /// Lowest value of the 2 steering inputs is how far both cables have been pulled down.
    /// Only the amount of which both handles are pulled brakes the parachute.
    /// </remarks>
    private void CalculateBrakeFactor()
    {
        m_brakeFactor = Mathf.Min(m_playerControl.Left, m_playerControl.Right);
    }


    /// <summary>
    /// Calculates the drag.
    /// </summary>
    /// <remarks>
    /// 
    /// </remarks>
    private void CalculateDrag ()
	{
        var speedFactor = m_playerControl.Rig.velocity.magnitude / 10f;
        m_playerControl.Rig.drag = (m_playerControl.Rig.velocity.magnitude * m_dragFactor + m_parachuteDrag) + m_brakeFactor * speedFactor;
    }
    

    /// <summary>
    /// Calculates the external forces on the player.
    /// </summary>
    /// <remarks>
    /// Calculates the lift and the forward force. The lift is perpendicular to the velocity and in the same plane that is perpendicular to the right direction.
    /// The forward force is to give the player more control of in which direction the player falls.
    /// </remarks>
    private void CalculateForces()
	{
        ///////////////////////////
        //CALCULATE LINEAR FORCES//
        ///////////////////////////

        m_forces = Vector3.zero; // Reset the forces vector to recalculate for this timestep.
        
        // Direction of Lift force is the local up.
        Vector3 liftDirection = Vector3.Cross(m_playerControl.Rig.velocity.normalized, m_playerControl.transform.right);
        // Calculate and add the lift force.
        float liftPower = ForwardSpeed * ForwardSpeed * m_liftForceFactor;
        //float liftPower = ForwardSpeed * m_playerControl.Rig.velocity.magnitude * m_liftForceFactor;
		Vector3 liftForce = liftPower * liftDirection;
        m_forces += liftForce;
        
        // Calculate the thrust force. Thrust force depends on the weight of the parachutist and the canopy, and the angle of inclination
        Vector3 thrustDirection = m_playerControl.transform.forward;
        // Calculate and add thrust force.
        float thrustPower = m_thrustFactor * m_playerControl.Rig.velocity.magnitude * (2.5f + Mathf.Abs(m_rollAngle) - Mathf.Abs(m_pitchAngle));
        Vector3 thrustForce = thrustPower * thrustDirection;
        m_forces += thrustForce;
        
        // Apply the calculated forces to the the Rigidbody.
        m_playerControl.Rig.AddForce(m_forces);
    }

    private void CalculateForwardSpeed()
    {
        var localVelocity = m_playerControl.transform.InverseTransformDirection(m_playerControl.Rig.velocity);
        ForwardSpeed = Mathf.Abs(localVelocity.z);
    }

    /// <summary>
    /// Rotates the player.
    /// </summary>
    /// <remarks>
    /// Rotation is done through scripting instead of calculated physics. Physics result in difficult to control behaviour which is bad for the game experience
    /// </remarks>
    private void RotatePlayer()
	{
		// Roll
		float RollInput = (m_playerControl.Left - m_playerControl.Right); // Rolling is only happening when one of the inputs is larger than the other, else would be braking.
		m_playerControl.transform.Rotate(Vector3.forward, RollInput + m_rollAngle); // Rotate accoring to the steering input and adjust the amount with the current roll angle to stop spinning entire circles.

        // Yaw
        // Caution should be taken when rotating players along this axis in Virtual reality, this axis is intensely VR sickness inducing.
        m_playerControl.transform.Rotate(Vector3.up, (0.5f * m_rollAngle - 0.5f * RollInput) * m_yawFactor, Space.World); // Rotate faster the higher the Roll angle. Rotation axis is upwards in world coördinates to ensure the player steadily and predictably rotates.
                
        // Pitch
        float speedFactor = (m_playerControl.Rig.velocity.magnitude / 10) * (0.2f + 0.8f * m_brakeFactor);
        float rotationFactor = speedFactor + m_pitchAngle;
        
        m_playerControl.transform.Rotate(m_playerControl.transform.right, 0.4f - rotationFactor, Space.World);
    }

    /// <summary>
    /// Controls the player animation for this state.
    /// </summary>
    /// <remarks>
    /// Sets the Animator parameters and layer weights for additive animating.
    /// </remarks>
    private void ControlPlayerAnimationState()
	{
        // Value is interpolated between the current value and the input value for smooth animations.
        // Not interpolating results in jerking between animations.
        m_anim.SetFloat("Left", Mathf.Lerp(m_anim.GetFloat("Left"), m_playerControl.Left, 0.1f));
		m_anim.SetFloat("Right", Mathf.Lerp(m_anim.GetFloat("Right"), m_playerControl.Right, 0.1f));
		m_anim.SetLayerWeight(1, m_anim.GetFloat("Left"));
		m_anim.SetLayerWeight(2, m_anim.GetFloat("Right"));
	}
}