﻿public class UIMenuButton : UIItem {


    protected override void HandleClick()
    {
        m_gameMaster.OnVRClick(m_button);
    }


    protected override void HandleOver()
    {
        base.HandleOver();
        m_gameMaster.OnVRHover();
    }
}
