﻿using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;


public class GroundBehaviour : StateMachineBehaviour, IPlayerState {

	static int landingState = Animator.StringToHash("Base.Landing");

	private RaycastHit hit;
	private PlayerController m_playerControl;
	private Animator m_anim;
    private PfdController m_pfd;

    // OnStateEnter is called before OnStateEnter is called on any state inside this state machine
    override public void OnStateEnter(Animator animator, AnimatorStateInfo stateInfo, int layerIndex) {

		// Assign private variables
		m_playerControl = animator.transform.gameObject.GetComponent<PlayerController>();
		m_anim = animator;
        m_pfd = m_playerControl.GetPFD();
        m_pfd.Grounded = true;

        // Add State to active state list
        var activeStates = animator.GetComponent<ActiveStates>();
		activeStates.list.Add(this);
	}

    // OnStateUpdate is called before OnStateUpdate is called on any state inside this state machine
    override public void OnStateUpdate(Animator animator, AnimatorStateInfo stateInfo, int layerIndex) {
		EvaluateState ();
		Behaviour ();
	}

	// OnStateExit is called before OnStateExit is called on any state inside this state machine
	override public void OnStateExit(Animator animator, AnimatorStateInfo stateInfo, int layerIndex) {
            
        var activeStates = animator.GetComponent<ActiveStates> ();
		activeStates.list.Remove (this);
		if (activeStates.list.Count > 0)
		{
			activeStates.list[0].OnTransitionComplete(animator, stateInfo, layerIndex);
		}
    }
    public void OnTransitionComplete(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
	{
		// Reset Mecanim state trigger
		m_anim.ResetTrigger("Landing");

        // Move player through animation
        animator.applyRootMotion = true;
		m_playerControl.ResetDrag();

        // Turn off direction canvas
        m_pfd.DirectionCanvas.SetActive(false);

        // Set height and score canvases at a readable angle for horizontal viewing
        m_pfd.HeightCanvas.transform.localRotation = Quaternion.Euler(new Vector3(-5f, -20f, 0f));
        m_pfd.ScoreCanvas.transform.localRotation = Quaternion.Euler(new Vector3(-5f, 20f, 0f));

        // Reset layer weights
        m_anim.SetLayerWeight(1, 0);
		m_anim.SetLayerWeight(2, 0);

        // Stop wind sound
        m_playerControl._windSrc.Stop();

        // If the completed transition is the Landing state, reset the rotation of the player
        if (animator.GetCurrentAnimatorStateInfo(0).IsName("Landing"))
			m_playerControl.ResetRotation();
	}
		
	private void EvaluateState ()
	{
		if (m_playerControl.Altitude > 5.0f && !m_playerControl.AboveGround())
		{
            MonoBehaviour.print("Set Trigger Freefall!");
            m_anim.SetTrigger("Freefall");
		}
	}
		
	private void Behaviour ()
	{
		ControlPlayerAnimationState();
        RotatePlayer();
	}

	private void ControlPlayerAnimationState ()
	{
        // Input handling for ground behaviour
        m_anim.SetFloat("Forward", Mathf.Lerp(m_anim.GetFloat("Forward"), m_playerControl.Forward, 0.1f));
		m_anim.SetFloat("Strafe", Mathf.Lerp(m_anim.GetFloat("Strafe"), m_playerControl.Strafe, 0.1f));
		m_anim.SetBool("Jump", m_playerControl.Jump);
		m_anim.SetBool("Run", m_playerControl.Run);
	}


    private void RotatePlayer()
    {
        // Rotate the player around the vertical axis whilst on the ground. This works well in combination with 2D freeform strafing animation
        float l_turnSpeedFactor = 3.5f;
        m_playerControl.transform.Rotate(Vector3.up, m_playerControl.Turn * l_turnSpeedFactor, Space.World);
    }
}