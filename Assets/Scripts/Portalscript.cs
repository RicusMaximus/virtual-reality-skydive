﻿using UnityEngine;
using System.Collections;

public class Portalscript : MonoBehaviour
{

    /* Members */
    // Private members
    private int _points = 1000;
    private AudioSource _ambient;
    [SerializeField] private GameObject _player;
    [SerializeField] private float _speed = 5.0f;
    [SerializeField] private GameObject _frontPortal;
    [SerializeField]
    private GameObject _backPortal;
    [SerializeField]
    private GameObject _particles;
    private bool _init = false;

    // Encapsulation
    public int Points { get { return _points; } set { _points = value; } }
    
    /* Methods */
    void Start()
    {
        _player = GameObject.Find("Player");
        _ambient = GetComponent<AudioSource>();
    }
    void Update ()
    {
        if (_ambient.time < 3.0f)
            _ambient.mute = true;
        if (_ambient.mute && _ambient.time > 3.0f)
            _ambient.mute = false;

        var dist = _player.transform.position - transform.position;
        if (dist.magnitude <= 1000)
        {
            PortalBehaviour();
            if (!_init)
            {
                _ambient.Play();
                _particles.SetActive(true);
                _init = true;
            }
        }
    }

    void PortalBehaviour()
    {
        _frontPortal.transform.Rotate(Vector3.forward, _speed * 5 * Time.deltaTime, Space.Self);
        _backPortal.transform.Rotate(Vector3.forward, -_speed * 5 * Time.deltaTime, Space.Self);
        
    }
}