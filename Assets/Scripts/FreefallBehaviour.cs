﻿// FreefallBehaviour.cs
using UnityEngine;
using System.Collections;

/*
* TO ADD
* Diving:
* Faster falling speed when diving
*/

/// <summary>
/// This Behaviour class controls the Freefall Sub State.
/// </summary>
/// <remarks>
/// This class controls the animation, physics, external forces and state machine behaviour during freefall.
/// </remarks>
public class FreefallBehaviour : StateMachineBehaviour, IPlayerState
{
    private RaycastHit hit; /// <summary>A RaycastHit object that holds information on the hit object.</summary>
    private PlayerController m_playerControl; /// <summary>The reference to the PlayerController script.</summary>
    private PlayerInput m_input; /// <summary>The reference to the PlayerInput script.</summary>
    private PfdController m_pfd; /// <summary>The reference to the Primary Flight Display Controller script.</summary>
    private Animator m_animator; /// <summary>The animator attached to this object.</summary>
    private Vector3 m_forces; /// <summary>The forces vector.</summary>
    private float m_forwardSpeed; /// <summary>The forward speed value.</summary>
    private float m_forwardForceFactor = 60.0f; /// <summary>the forward force balancing factor.</summary>
    private float m_bankForceFactor = 40.0f; /// <summary>The bank force balancing factor.</summary>
    private float m_yawFactor = 0.01f; /// <summary>The yaw rotation balancing factor.</summary>
    private float m_liftFactor = 0.001f; /// <summary></summary>
    private float m_automaticActivationDevice = 30.0f; /// <summary>Automatic Activation Device.</summary><remarks>A device that automatically opens the parachute at a certain height.</remarks>

    // OnStateEnter is called before OnStateEnter is called on any state inside this state machine.
    override public void OnStateEnter(Animator animator, AnimatorStateInfo stateInfo, int layerIndex) {
            
		// Assign private variables
		m_playerControl = animator.transform.gameObject.GetComponent<PlayerController>();
		m_input = animator.transform.gameObject.GetComponent<PlayerInput>();
        m_animator = animator;
        m_pfd = m_playerControl.GetPFD();

        MonoBehaviour.print("Delay Freefall!");
        m_playerControl.FfToggled = true;
        m_playerControl.StartCoroutine(m_playerControl.DelayFreefall());

        // Add State to active state list
        var activeStates = animator.GetComponent<ActiveStates>();
		activeStates.list.Add(this);

	}

	// OnStateUpdate is called before OnStateUpdate is called on any state inside this state machine
	override public void OnStateUpdate(Animator animator, AnimatorStateInfo stateInfo, int layerIndex) {
		EvaluateState ();
		Behaviour ();
	}

	// OnStateExit is called before OnStateExit is called on any state inside this state machine
	override public void OnStateExit(Animator animator, AnimatorStateInfo stateInfo, int layerIndex) {
		var activeStates = animator.GetComponent<ActiveStates> ();
		activeStates.list.Remove (this);
		if (activeStates.list.Count > 0)
		{
			activeStates.list[0].OnTransitionComplete(animator, stateInfo, layerIndex);
		}
	}

	// OnStateMachineEnter is called when entering a statemachine via its Entry Node
	override public void OnStateMachineEnter(Animator animator, int stateMachinePathHash){
		animator.applyRootMotion = false;
	}

	public void OnTransitionComplete(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
	{
        MonoBehaviour.print("Reset Trigger Freefall!");
        m_animator.ResetTrigger("Freefall");
        m_playerControl.ResetRotation();
        m_playerControl._windSrc.Play();
        m_pfd.Grounded = false;

        // Turn on direction canvas
        m_pfd.DirectionCanvas.SetActive(true);

        // Set height and score canvases at a readable angle for looking down
        m_pfd.HeightCanvas.transform.localRotation = Quaternion.Euler(new Vector3(40f, -25f, -15f));
        m_pfd.ScoreCanvas.transform.localRotation = Quaternion.Euler(new Vector3(40f, 25f, 15f));
    }
    
	private void EvaluateState ()
	{
        if (!m_playerControl.FfToggled)
        {
            // IF parachute is opened, call method toparachutestate
            if (m_playerControl.TogglePara)
            {
                MonoBehaviour.print("Set Trigger OpenParachute!!");
                m_animator.SetTrigger("OpenParachute");
            }
        }
        if (m_playerControl.transform.position.y <= m_automaticActivationDevice || m_playerControl.Altitude <= 6.0f)
        {
            MonoBehaviour.print("Set Trigger OpenParachute!");
            m_animator.SetTrigger("OpenParachute");
        }
	}


	private void Behaviour()
	{
		CalculateDrag();
		CalculateForwardSpeed();
		CalculateForces();
        CalculateRotation();
		ControlPlayerAnimationState();
	}

    //
    // Summary:
    //     ///
    //     Drag is proportional to falling speed, a constant factor and the original drag value
    //     ///
    private void CalculateDrag ()
	{
		m_playerControl.Rig.drag = m_playerControl.Rig.velocity.magnitude * 0.005f + m_playerControl.OriginalDrag;
		m_playerControl.Rig.angularDrag = 0.1f + m_forwardSpeed * 0.01f;
	}

	private void CalculateForwardSpeed()
	{
		// Forward speed is the speed in the player's forward direction (not the same as its velocity)
		var localVelocity = m_playerControl.transform.InverseTransformDirection(m_playerControl.Rig.velocity);
		m_forwardSpeed = Mathf.Max(0, localVelocity.z);
	}

	private void CalculateForces()
	{
		// Now calculate forces acting on the player:
		// we accumulate forces into this variable:
		m_forces = Vector3.zero;

		m_forces += m_playerControl.Forward * m_playerControl.transform.forward * m_forwardForceFactor;
			
		// Direction of m_liftFactor force is up
		var liftDirection = Vector3.Cross(m_playerControl.Rig.velocity, m_playerControl.transform.right).normalized;
			
		// Calculate and add the lift power
		var liftPower = m_forwardSpeed * m_forwardSpeed * m_liftFactor;
		m_forces += liftPower * liftDirection;

		// Add sideways force when turning
		m_forces += m_playerControl.Turn * m_playerControl.Turn * m_playerControl.transform.right;
            
        // Bank force
            if (!m_playerControl.Run)
            m_forces += m_playerControl.Strafe * m_playerControl.transform.right * m_bankForceFactor;

        // Apply the calculated forces to the the Rigidbody
        m_playerControl.Rig.AddForce(m_forces * 10);
	}

    private void CalculateRotation()
    {
        float turnAmount;
        #region Yaw
        turnAmount = m_playerControl.Turn / (1 + m_playerControl.Rig.angularDrag);
        // Rotate player around up-axis
        m_playerControl.Rig.rotation *= Quaternion.Euler(new Vector3(0, turnAmount, 0));
        #endregion
    }

	private void ControlPlayerAnimationState()
	{
		m_animator.SetFloat("Forward", Mathf.Lerp(m_animator.GetFloat("Forward"), m_playerControl.Forward, 0.075f));
		m_animator.SetFloat("Turn", Mathf.Lerp((m_animator.GetFloat("Turn")), m_playerControl.Turn + m_playerControl.Bank, 0.075f));
		m_animator.SetBool("Run", m_playerControl.Run);
		m_animator.SetBool("Open", m_playerControl.TogglePara);
	}
}