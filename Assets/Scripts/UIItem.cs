﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;

public abstract class UIItem : MonoBehaviour
{
    protected bool m_over;
    protected float m_speed = 0.6f;
    protected GameMaster m_gameMaster;
    protected HighscoreManager m_highscoreManager;
    protected RectTransform m_rect;
    protected IEnumerator m_IEnumUp, m_IEnumDown;
    protected UnityEngine.UI.Button m_button;
    protected VRStandardAssets.Utils.VRInteractiveItem m_interactiveItem;

    protected void Awake()
    {
        m_rect = GetComponent<RectTransform>();
        m_interactiveItem = GetComponent<VRStandardAssets.Utils.VRInteractiveItem>();
        m_button = GetComponent<UnityEngine.UI.Button>();
        m_gameMaster = GameObject.Find("GameMaster").GetComponent<GameMaster>();
        m_highscoreManager = GameObject.Find("HighscoreManager").GetComponent<HighscoreManager>();
    }
    
    protected void OnEnable()
    {
        m_interactiveItem.OnOver += HandleOver;
        m_interactiveItem.OnOut += HandleOut;
        m_interactiveItem.OnClick += HandleClick;
    }


    protected void OnDisable()
    {
        m_interactiveItem.OnOver -= HandleOver;
        m_interactiveItem.OnOut -= HandleOut;
        m_interactiveItem.OnClick -= HandleClick;
    }


    protected virtual void HandleOver()
    {
        if (!m_over)
        {
            m_IEnumUp = Scale(1.1f);
            if (m_IEnumDown != null)
                StopCoroutine(m_IEnumDown);
            StartCoroutine(m_IEnumUp);
            m_over = true;
        }
    }


    protected virtual void HandleOut()
    {
        if (m_over)
        {
            m_IEnumDown = Scale(1f);
            if (m_IEnumUp != null)
                StopCoroutine(m_IEnumUp);
            StartCoroutine(m_IEnumDown);
            m_over = false;
        }
    }

    protected abstract void HandleClick();

    protected IEnumerator Scale(float a_size)
    {
        if (a_size == 1.1f)
        {
            while (m_rect.localScale.y < a_size)
            {
                m_rect.localScale += new Vector3(0.01f, 0.01f, 0.01f);
                yield return StartCoroutine(CoroutineUtil.WaitForRealSeconds(0.01f));
            }
        }
        else
        {
            while (m_rect.localScale.y > a_size)
            {
                m_rect.localScale -= new Vector3(0.01f, 0.01f, 0.01f);
                yield return StartCoroutine(CoroutineUtil.WaitForRealSeconds(0.01f));
            }
        }
    }
    
}