﻿// GameMaster.cs
using UnityEngine;
using UnityEngine.UI;
using System;
using System.IO;
using System.Text.RegularExpressions;
using UnityStandardAssets.CrossPlatformInput;
using UnityEngine.Audio;
using System.Collections;

//
// The semi-god class that controls the game
public class GameMaster : MonoBehaviour
{
    [SerializeField] private Font m_font;   // The game font
    [SerializeField] private AudioMixerGroup _musicGroup, _SFXGroup; // Audio mixers

    private GameManager GM;                 // Reference to the Game Manager instance
    private HighscoreManager HM;            // Reference to the Highscore manager

    private IEnumerator m_intro;

    private bool m_pauseGame = false;       // Is the game paused or not?

    [SerializeField] private Button[] m_levelButtons;        // The list of buttons for selecting the level time mode
    private Text m_messageText;             // The error message Text object
    private string m_message;               // The error message displayed when the name is incorrect
    private PlayerData m_playerData;        // The player data object
    //private int Level = 2;                   // The current level index

    public GameObject GameOverCanvas, PauseCanvas, MainMenu, HighscoreMenu;  // The menus
    public CanvasGroup ScoreCanvas, PitchCanvas, HeightCanvas, DistanceCanvas, GameOverCanvasInstance, HighscoreCanvas, PauseCanvasInstance;
    public Text ScoreText;
    public Transform m_player;              // Tranformation data of the player
    public string m_playerName;             // The name of the gamer
    public int GameTimeMode = 3;     // The duration of the current game, is standard 3 minutes
    public AudioClip HoverClip, ClickClip;  // Audio Clips
    public AudioSource AudioCenterSrc;      // Audio Sources

    public PlayerData PlayerData { get { return m_playerData; } set { m_playerData = value; } }

    private Color m_activeColor = new Color(1, 0, 224f / 255f), m_passiveColor = new Color(0.1f, 0.1f, 0.1f);


    // Use this for initialization
    private void Awake()
    {
        if (Application.loadedLevel == 0) return;

        AudioCenterSrc = AddAudio(gameObject, null, _SFXGroup, false, false, 1f, 1);
        m_levelButtons = GameObject.Find("LevelButtons").GetComponentsInChildren<Button>();
        HM = GameObject.Find("HighscoreManager").GetComponent<HighscoreManager>();
        SetFont(m_font);
    }

    private void Start ()
    {
        GM = GameManager.Manager;
        GM.OnStateChange += HandleOnStateChange;
        GM.SetGameState(GameState.INTRO);
        m_intro = Intro();
        StartCoroutine(m_intro);
    }

    private IEnumerator Intro ()
    {
        yield return new WaitForSeconds(3);
        GM.SetGameState(GameState.MAIN_MENU);
        Application.LoadLevel(1);
    }

    public void HandleOnStateChange()
    {
        //print("OnStateChange!");
        //print(GM.gameState);
    }


    // Update is called once per frame
    private void Update()
    {
        ////////////////////////
        // USE INPUT MANAGER! //
        ////////////////////////
        if (CrossPlatformInputManager.GetButtonDown("Cancel") && GM.gameState != GameState.MAIN_MENU)
        {
            if (!m_pauseGame)
                PauseGame();
            else if (m_pauseGame)
                UnpauseGame();
        }
        if (CrossPlatformInputManager.GetButtonDown("ResetTracking")) { UnityEngine.VR.InputTracking.Recenter(); }
    }

    public void OnVRHover()
    {
        AudioCenterSrc.PlayOneShot(HoverClip, 0.3f);
    }

    
    public void OnVRClick(Button button)
    {
        AudioCenterSrc.PlayOneShot(ClickClip, 0.5f);
        
        switch (button.name)
        {
            case "Resume":
                UnpauseGame();
                break;
            case "Restart":
                Restart();
                break;
            case "Return":
                ReturnToMenu();
                break;
            case "Start":
                StartGame();
                break;
            case "Tutorial":
                StartTutorial();
                break;
            case "Options":
                //ShowOptions();
                break;
            case "Highscores":
                ToggleHighscores();
                break;
            case "Credits":
                //ShowCredits();
                break;
            case "Exit":
                Quit();
                break;
            case "3":
                GameTimeMode = 3;
                SetActiveLevelButton(button);
                break;
            case "5":
                GameTimeMode = 5;
                SetActiveLevelButton(button);
                break;
            case "8":
                GameTimeMode = 8;
                SetActiveLevelButton(button);
                break;
        }
    }

    private void SetActiveLevelButton(Button button)
    {
        if (button.GetComponent<UnityEngine.UI.Image>().color == m_activeColor)
        {
            print("Button is already active!");
            return; // Do not handle this function if the clicked button is already active
        }
        
        foreach (Button btn in m_levelButtons)
        {
            btn.GetComponent<Image>().color = m_passiveColor;
        }
        button.GetComponent<Image>().color = m_activeColor;

        HM.LoadScores();
    }

    public void StartGame()
    {
        //start game scene
        GM.SetGameState(GameState.GAME);
        Application.LoadLevel(2);
    }

    private void ReturnToMenu()
    {
        if (GM.gameState == GameState.PAUSED)
        {
            Destroy(GameObject.Find("PauseCanvas(Clone)"));
        }
        Time.timeScale = 1f;
        
        GM.SetGameState(GameState.MAIN_MENU);
        Application.LoadLevel(1);
    }

    private void Init ()
    {
        AudioCenterSrc = AddAudio(gameObject, null, _SFXGroup, false, false, 1f, 1);
        m_levelButtons = GameObject.Find("LevelButtons").GetComponentsInChildren<Button>();
        HM = GameObject.Find("HighscoreManager").GetComponent<HighscoreManager>();
        SetFont(m_font);
    }

    void OnLevelWasLoaded (int levelIndex)
    {
        if (levelIndex == 0) return; // Do not execute at intro
        
        print("OnLevelWasLoaded!");

        if (levelIndex != 1) // Call functions when the level is not Main Menu
        {
            SetGameReferences();
            SetFont(m_font);
            SetStartPosition();
        }
        else
        {
            GM = GameManager.Manager;
            //GM.OnStateChange += HandleOnStateChange;
            //GM.SetGameState(GameState.MAIN_MENU);
            GM.Loaded = true; // Werkt voor nu, is wel een matige oplossing
            Init();
            HighscoreMenu = GameObject.Find("HighscoreMenu");
            HighscoreCanvas = HighscoreMenu.GetComponent<CanvasGroup>();
            HM = GameObject.Find("HighscoreManager").GetComponent<HighscoreManager>();
            m_levelButtons = GameObject.Find("LevelButtons").GetComponentsInChildren<Button>();

            // Set active game time mode button
            switch (GameTimeMode)
            {
                case 3:
                    print(m_levelButtons[0]);
                    SetActiveLevelButton(m_levelButtons[0]);
                    break;
                case 5:
                    print(m_levelButtons[1]);
                    SetActiveLevelButton(m_levelButtons[1]);
                    break;
                case 8:
                    print(m_levelButtons[2]);
                    SetActiveLevelButton(m_levelButtons[2]);
                    break;
                default: break;
            }
        }
    }

    private void SetStartPosition()
    {
        int height = 3000;
        switch (GameTimeMode)
        {
            case 3:
                height = 3000;
                GameObject.Find("Level5").SetActive(false);
                GameObject.Find("Level8").SetActive(false);
                break;
            case 5:
                height = 5000;
                GameObject.Find("Level8").SetActive(false);
                break;
            case 8:
                height = 8000;
                break;
            default: throw new Exception("GameTimeMode has an unavailable value!");
        }
        GameObject startbuilding = GameObject.Find("Startbuilding");
        GameObject centerspire = GameObject.Find("Center Spire");

        m_player.transform.position = new Vector3(0, height, 0);

        startbuilding.transform.position = new Vector3(0, height, 0);

        centerspire.transform.position = new Vector3(0, height / 2, 0);
        centerspire.transform.localScale = new Vector3(10, height - 10, 10);
    }

    private void SetGameReferences()
    {
        foreach (GameObject cvg in GameObject.FindGameObjectsWithTag("Reference"))
        {
            switch (cvg.name)
            {
                case "PitchCanvas":
                    PitchCanvas = cvg.GetComponent<CanvasGroup>();
                    break;
                case "ScoreCanvas":
                    ScoreCanvas = cvg.GetComponent<CanvasGroup>();
                    break;
                case "HeightCanvas":
                    HeightCanvas = cvg.GetComponent<CanvasGroup>();
                    break;
                case "DistanceCanvas":
                    DistanceCanvas = cvg.GetComponent<CanvasGroup>();
                    break;
            }   
        }
        m_player = GameObject.Find("Player").transform;
    }

    public void StartTutorial()
    {

        //start game scene
        GM.SetGameState(GameState.TUTORIAL);
        print(GM.gameState);
    }

    private void Restart()
    {
        if (m_pauseGame)
            UnpauseGame();
        Application.LoadLevel(Application.loadedLevel);
    }

    private void PauseGame()
    {
        m_pauseGame = true;
        GM.SetGameState(GameState.PAUSED);
        Time.timeScale = 0f;
        PauseCanvasInstance = Instantiate(PauseCanvas, m_player.position + 200 * m_player.forward + 10 * -transform.up, m_player.rotation) as CanvasGroup;
        print(GM.gameState);
    }

    private void UnpauseGame()
    {
        GM.SetGameState(GameState.GAME);
        m_pauseGame = false;
        Time.timeScale = 1f;
        Destroy(GameObject.Find("PauseCanvas(Clone)"));
    }

    private void ToggleHighscores()
    {
        if (HighscoreCanvas.alpha > 0.5f)
        {
            ToggleMenuVisibility(0f, HighscoreCanvas);
            ToggleMenuCollidersOff(HighscoreCanvas);
        }
        else
        {
            ToggleMenuVisibility(1f, HighscoreCanvas);
            ToggleMenuCollidersOn(HighscoreCanvas);
        }
    }
    
    public void GameOver()
    {
        print("Het spel duurde " + Time.timeSinceLevelLoad.ToString() + " seconden!");
        ToggleMenuVisibility(0f, ScoreCanvas); ToggleMenuCollidersOff(ScoreCanvas);
        ToggleMenuVisibility(0f, PitchCanvas); ToggleMenuCollidersOff(PitchCanvas);
        ToggleMenuVisibility(0f, HeightCanvas); ToggleMenuCollidersOff(HeightCanvas);
        ToggleMenuVisibility(0f, DistanceCanvas); ToggleMenuCollidersOff(DistanceCanvas);

        // Create Game Over menu in front of player
        GameOverCanvasInstance = Instantiate(GameOverCanvas, m_player.position + 200 * m_player.forward + 10 * transform.up, m_player.rotation) as CanvasGroup;
        GameOverCanvasInstance.transform.parent = null;
        ToggleMenuVisibility(1f, GameOverCanvasInstance); ToggleMenuCollidersOn(GameOverCanvasInstance);

        foreach (Text txt in GameOverCanvasInstance.GetComponentsInChildren<Text>())
        {
            if (txt.name == "Points")
                ScoreText = txt;
        }

        m_messageText = GameOverCanvasInstance.transform.Find("ErrorMessage").GetComponent<Text>(); // Set the error message object reference after instantiation

        // Stop the player from moving
        m_player.GetComponent<Animator>().enabled = false;
        ScoreText.text = PlayerData.Score.ToString();
    }

    ///
    /// <summary>
    ///     Add the current PlayerData object to the Xml file with the name, date and score
    /// </summary>
    private void AddPlayer()
    {
        m_playerData.Name = m_playerName;
        m_playerData.Date = DateTime.Now;
        m_playerData.LevelTime = GameTimeMode;
        // add Player to collection
        var playerCollection = PlayerDataContainer.Load(Path.Combine(Application.dataPath, "players.xml"));
        playerCollection.Players.Add(PlayerData);
        playerCollection.Save(Path.Combine(Application.dataPath, "players.xml"));

        // Disable submit button and input field
        GameOverCanvasInstance.transform.Find("Submit").gameObject.SetActive(false);
        GameOverCanvasInstance.transform.Find("InputField").gameObject.SetActive(false);

        // Enable return button
        GameOverCanvasInstance.transform.Find("Return").gameObject.SetActive(true);
        GameOverCanvasInstance.transform.Find("Return").gameObject.GetComponent<BoxCollider>().enabled = true;
    }


    private void Quit()
    {
        Application.Quit();
    }

    // Static methods

    /// <summary>
    ///     Toggles a GUI Menu's interactability and visibility
    /// </summary>
    /// <param name="a">Alpha value of CanvasGroup</param>
    /// <param name="menu">CanvasGroup reference</param>
    public static void ToggleMenuVisibility(float a, CanvasGroup menu)
    {
        menu.alpha = a;
    }
    public static void ToggleMenuCollidersOn(CanvasGroup menu)
    {
        foreach (BoxCollider box in menu.GetComponentsInChildren<BoxCollider>())
        { box.enabled = true; }
    }
    public static void ToggleMenuCollidersOff(CanvasGroup menu)
    {
        foreach (BoxCollider box in menu.GetComponentsInChildren<BoxCollider>())
        { box.enabled = false; }
    }

    public static AudioSource AddAudio(GameObject obj, AudioClip clip, AudioMixerGroup group, bool loop, bool playAwake, float vol, int priority)
    {
        var newAudio = obj.AddComponent<AudioSource>();
        newAudio.outputAudioMixerGroup = group;
        newAudio.clip = clip;
        newAudio.loop = loop;
        newAudio.playOnAwake = playAwake;
        newAudio.volume = vol;
        newAudio.priority = priority;
        return newAudio;
    }

    public bool OnNameSubmit (string name)
    {
        Match result = ValidateName(name);
        if (result != null)
        {
            // Add player
            m_messageText.text = "";
            m_playerName = result.Value;
            AddPlayer();
            return true;
        }
        else
        {
            // Show error message
            m_messageText.text = m_message;
            return false; // Submit failed
        }
    }

    public static void SetFont(Font font)
    {
        foreach (Text txt in FindObjectsOfType<Text>())
            txt.font = font;
    }

    private Match ValidateName (string input)
    {
        string pattern = @"^[\p{L}\p{M}' \.\-]+$"; // Regular expression pattern for common name syntax
        Match result = Regex.Match(input, pattern); // Check the name for regular expression patterns

        if (!result.Success)
        {
            // Check error
            m_message = "Graag een geldige naam invullen"; // placeholder
            return null;
        }
        else return result;
    }
}