﻿using UnityEngine;
using System.Collections.Generic;
using System.Linq;
using System;

//
// This class manages the highscores
public class HighscoreManager : MonoBehaviour
{
    [SerializeField] private UnityEngine.UI.Button[] m_buttons;
    private UnityEngine.UI.Button m_activeButton;
    [SerializeField] private GameObject m_highscoreMenu;
    [SerializeField] private UnityEngine.UI.Text m_rankText, m_nameText, m_scoreText, m_titleText;
    private CanvasGroup m_highscoreCanvasGroup;
    private PlayerDataContainer m_playerCollection;
    private List<PlayerData> m_sortedScoreList;
    
    
    private GameMaster m_gameMaster; // The reference to the Game Master.

    /// <summary>
    /// The color for active buttons
    /// </summary>
    private Color m_activeColor = new Color(150f / 255f, 0, 132 / 255f), m_passiveColor = new Color(21 / 255f, 21 / 255f, 21 / 255f);
    
    void Start()
    {
        //Init(); // Initializes the manager and sets references.
    }

    private void OnLevelWasLoaded(int levelIndex)
    {
        if (levelIndex == 1)
        {
            Init();
            print("Init Highscoremanager!");
        }
    }

    private void Init()
    {
        // Find objects and set references
        m_gameMaster = GameObject.Find("GameMaster").GetComponent<GameMaster>();
        m_highscoreMenu = GameObject.Find("HighscoreMenu");
        m_highscoreCanvasGroup = m_highscoreMenu.GetComponent<CanvasGroup>();
        m_nameText = m_highscoreMenu.transform.Find("Canvas/Names").GetComponent<UnityEngine.UI.Text>();
        m_scoreText = m_highscoreMenu.transform.Find("Canvas/Scores").GetComponent<UnityEngine.UI.Text>();
        m_rankText = m_highscoreMenu.transform.Find("Canvas/Ranks").GetComponent<UnityEngine.UI.Text>();
        m_titleText = m_highscoreMenu.transform.Find("Canvas/HighscoreTitle").GetComponent<UnityEngine.UI.Text>();
        m_buttons = m_highscoreMenu.GetComponentsInChildren<UnityEngine.UI.Button>();
        m_activeButton = m_buttons[0];

        // Read all highscores
        m_playerCollection = PlayerDataContainer.Load(System.IO.Path.Combine(Application.dataPath, "players.xml"));

        // use Linq to load the list into an ordered list based on score
        m_sortedScoreList = m_playerCollection.Players.OrderByDescending(data => data.Score).ToList();


        // fill the text objects with the sorted list
        // First clear the text
        m_nameText.text = "";
        m_scoreText.text = "";
        m_rankText.text = "";
        int playerCount = 0;
        foreach (PlayerData player in m_sortedScoreList)
        {
            if (player.LevelTime == m_gameMaster.GameTimeMode)
            {
                playerCount++;
                m_nameText.text += player.Name.ToString() + "\n";
                m_scoreText.text += player.Score.ToString() + "\n";
                m_rankText.text += playerCount.ToString() + "\n";
            }
        }

        m_highscoreCanvasGroup.alpha = 0;
    }
    
    /// <summary>
    /// Called when the button is clicked while the Gaze of He Who Is In Virtual Reality shines upon said button.
    /// </summary>
    /// <param name="button">The button.</param>
    public void OnVRClick(UnityEngine.UI.Button button)
    {

        m_gameMaster.AudioCenterSrc.PlayOneShot(m_gameMaster.ClickClip, 0.5f);
        foreach (UnityEngine.UI.Button btn in m_buttons)
        {
            btn.GetComponent<UnityEngine.UI.Image>().color = m_passiveColor;
        }
        button.GetComponent<UnityEngine.UI.Image>().color = m_activeColor;

        m_activeButton = button;
        
        LoadScores();
    }

    public void LoadScores ()
    {
        switch (m_activeButton.name)
        {
            case "All-Time":
                // Set All time higschores active
                SetAllTimeHighscores();
                break;
            case "Month":
                // Set monthly higschores active
                SetMonthlyHighscores();
                break;
            case "Today":
                // Set daily higschores active
                SetDailyHighscores();
                break;
        }

        m_titleText.text = "Highscores " + m_gameMaster.GameTimeMode + "km";
    }

    private void SetDailyHighscores()
    {
        // Fill the text objects with the sorted list
        // First clear the text
        m_nameText.text = "";
        m_scoreText.text = "";
        m_rankText.text = "";

        int playerCount = 0;

        var today = DateTime.Now.Day;
        var thisMonth = DateTime.Now.Month;

        foreach (PlayerData player in m_sortedScoreList)
        {
            if (today == player.Date.Day && thisMonth == player.Date.Month)
            {
                if (player.LevelTime == m_gameMaster.GameTimeMode)
                {
                    playerCount++;
                    m_nameText.text += player.Name.ToString() + "\n";
                    m_scoreText.text += player.Score.ToString() + "\n";
                    m_rankText.text += playerCount.ToString() + "\n";
                }
            
            }
        }
    }

    private void SetMonthlyHighscores()
    {
        // Fill the text objects with the sorted list
        // First clear the text
        m_nameText.text = "";
        m_scoreText.text = "";
        m_rankText.text = "";

        int playerCount = 0;

        var thisMonth = DateTime.Now.Month;

        foreach (PlayerData player in m_sortedScoreList)
        {
            if (thisMonth == player.Date.Month)
            {
                if (player.LevelTime == m_gameMaster.GameTimeMode)
                {
                    playerCount++;
                    m_nameText.text += player.Name.ToString() + "\n";
                    m_scoreText.text += player.Score.ToString() + "\n";
                    m_rankText.text += playerCount.ToString() + "\n";
                }
            }
        }
    }

    private void SetAllTimeHighscores()
    {
        // Fill the text objects with the sorted list
        // First clear the text
        m_nameText.text = "";
        m_scoreText.text = "";
        m_rankText.text = "";

        int playerCount = 0;

        foreach (PlayerData player in m_sortedScoreList)
        {
            if (player.LevelTime == m_gameMaster.GameTimeMode)
            {
                playerCount++;
                m_nameText.text += player.Name.ToString() + "\n";
                m_scoreText.text += player.Score.ToString() + "\n";
                m_rankText.text += playerCount.ToString() + "\n";
            }
        }
    }
}
