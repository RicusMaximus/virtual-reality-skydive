﻿using UnityEngine;
using System.Collections.Generic;
using System.Collections;

public class Shutter : MonoBehaviour {

    [SerializeField] private List<Transform> m_shutters;
    private IEnumerator m_scaler;
    private bool m_open = false;
    private AudioSource m_shutterSound;

    void Awake ()
    {
        m_shutterSound = GetComponent<AudioSource>();
        foreach (Transform go in transform) { m_shutters.Add(go); } // Add all shutters to the list
        m_scaler = OpenShutters();                                  // Assign OpenShutters (method) to m_scaler (enumerator)
    }
    
	// Update is called once per frame
	void OnTriggerEnter (Collider other)
    {
        if (other.gameObject.name == "Player" && !m_open)
        {
            StartCoroutine(m_scaler);
            m_shutterSound.PlayOneShot(m_shutterSound.clip);
            m_open = true;
        }
    }

    private IEnumerator OpenShutters ()
    {
        float it = 1.0f;
        while (it > 0.1f)                              // While the scale iterator is still above 0.01, execute the loop
        {
            foreach (Transform go in m_shutters)
            {
                go.localScale = new Vector3(it, it, it);
            }
            it -= 0.01f;
            yield return new WaitForSeconds(0.01f);     // Wait 0.05 seconds before continuing loop
        }
    }
}
