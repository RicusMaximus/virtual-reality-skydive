﻿// PlayerDataContatiner.cs
using System.Collections.Generic;
using System.Xml;
using System.Xml.Serialization;
using System.IO;

/// <summary>
/// This class enables the game to read and write Xml files which contain player data
/// The player data is used for the highscores associated with text
/// </summary>
[XmlRoot("PlayerDataCollection")]
public class PlayerDataContainer
{
    /// <summary>
    /// The list of players.
    /// </summary>
    [XmlArray("Players"), XmlArrayItem("PlayerData")]
    public List<PlayerData> Players = new List<PlayerData>();

    /// <summary>
    /// Initializes a new instance of the <see cref="PlayerDataContainer"/> class.
    /// </summary>
    public PlayerDataContainer() { }


    /// <summary>
    /// Save the playerdata to the specified path.
    /// </summary>
    /// <param name="path">The path.</param>
    public void Save(string path)
    {
        /*
        XmlSerializer serializer = new XmlSerializer(typeof(PlayerDataContainer));
        using (FileStream stream = new FileStream(path, FileMode.Create))
        {
            serializer.Serialize(stream, this);
        }

        */

        XmlSerializer serializer = new XmlSerializer(typeof(PlayerDataContainer));
        string filename = path;
        var encoding = System.Text.Encoding.GetEncoding("UTF-8");
        using (StreamWriter stream = new StreamWriter(filename, false, encoding))
        {
            serializer.Serialize(stream, this);
        }

    }


    /// <summary>
    /// Loads the file from the specified path.
    /// </summary>
    /// <param name="path">The path.</param>
    /// <returns>Returns the contents of the Xml file as a PlayerDataContainer object</returns>
    public static PlayerDataContainer Load(string path)
    {
        // Check if path is valid and file exists
        if (!File.Exists(path))
        {
            UnityEngine.MonoBehaviour.print("File does not exist, creating a new one");
            File.Create(path);
            PlayerDataContainer pdc = new PlayerDataContainer();
            UnityEngine.MonoBehaviour.print(pdc);
            UnityEngine.MonoBehaviour.print(pdc.Players);
            pdc.Save(path);
        }

        
        var serializer = new XmlSerializer(typeof(PlayerDataContainer));
        using (var stream = new FileStream(path, FileMode.Open))
        {
            return serializer.Deserialize(stream) as PlayerDataContainer;
        }
    }


    /// <summary>
    /// Loads from text string.
    /// </summary>
    /// <remarks>
    /// Loading directly from a given string is useful in combination with WWW.
    /// </remarks>
    /// <param name="text">The text.</param>
    /// <returns>Returns the contents of the Xml file as a PlayerDataContainer object</returns>
    public static PlayerDataContainer LoadFromText(string text)
    {
        var serializer = new XmlSerializer(typeof(PlayerDataContainer));
        return serializer.Deserialize(new StringReader(text)) as PlayerDataContainer;
    }
}
