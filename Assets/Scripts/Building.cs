﻿// Building.cs
using UnityEngine;
using System;

//
// This class that contains the methods for the buildings.
public enum BuildingValue {
    /// <summary>Low building value.</summary>
    LAGE,
    /// <summary>Medium building value.</summary>
    MIDDEL,
    /// <summary>High building value.</summary>
    HOGE,
    /// <summary>Extremely high building value.</summary>
    EXTREME
}

/// <summary>
/// This class holds the information and behaviour of a building
/// </summary>
public class Building : MonoBehaviour {
    
    [SerializeField] private int m_points;
    [SerializeField] private float m_areaValue;
    [SerializeField] private GameObject m_player;
    [SerializeField] private BuildingValue m_value;
    private Material m_mat;

    public int Points { get { return m_points; } }


    public BuildingValue Value { get { return m_value; } }


    //
    // Initializes this instance.
    void Awake()
    {
        m_mat = GetComponent<Renderer>().material;
        m_player = GameObject.Find("Player");
    }


    //
    // Starts this instance.
    void Start()
    {
        CalculateValue(); // CalculateValue has to be called before SetColor as SetColor depends on the calculated value
        SetColor();
        gameObject.name = SetName(m_value);
    }

    /// <summary>
    /// Calculates the value of the building.
    /// </summary>
    /// <exception cref="System.ArgumentOutOfRangeException">Throws an Argument Out Of Range Exception if the amount of points is invalid.</exception>
    void CalculateValue()
    {
        m_points = 10;
        var distToTop = m_player.transform.position - (transform.position + new Vector3(0, transform.localScale.y / 2, 0f));
        
        if (distToTop.y > m_player.transform.position.y - 100f)
        {
            // Add more points the Lower the building is under 100m
            m_points += (int)distToTop.y;
        }
        
        distToTop.y = 0;
        m_areaValue = (transform.localScale.x * transform.localScale.z)/100f;

        m_points += (int)Mathf.Round(Mathf.Pow(distToTop.magnitude, 1.5f) / m_areaValue);
        if (m_points > 0)
        {

            if (m_points < 3000f)
            {
                m_value = BuildingValue.LAGE;
            }
            else if (m_points >= 3000 && m_points < 6000)
            {
                m_value = BuildingValue.MIDDEL;
            }
            else if (m_points >= 6000 && m_points < 20000)
            {
                m_value = BuildingValue.HOGE;
            }
            else if (m_points >= 20000)
            {
                m_value = BuildingValue.EXTREME;
                if (m_points > 70000)
                    m_points = 70000;
            }

        }
        else throw new ArgumentOutOfRangeException();
    }

    /// <summary>
    /// Sets the color of the building that corresponds with it's value.
    /// </summary>
    /// <exception cref="System.ArgumentException">An exception is thrown when the value is not a valid BuildingValue value</exception>
    private void SetColor()
    {
        switch (Value)
        {
            case BuildingValue.LAGE:
                m_mat.color = new Color(0.2f, 0.2f, 0.2f, 1f);
                break;
            case BuildingValue.MIDDEL:
                m_mat.color = new Color(28f / 255f, 193f / 255f, 152f / 255f, 1f);
                break;
            case BuildingValue.HOGE:
                m_mat.color = new Color(4f / 255f, 104f / 255f, 255f / 255f, 1f);
                break;
            case BuildingValue.EXTREME:
                m_mat.color = new Color(1f, 0f, 1f, 1f);
                break;
            default:
                throw new ArgumentException();
        }
    }

    private string SetName(BuildingValue arg_value)
    {
        return "GEBOUW (" + arg_value.ToString() + " WAARDE)";
    }
}
