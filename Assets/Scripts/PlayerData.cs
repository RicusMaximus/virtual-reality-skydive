﻿// PlayerData.cs
using System;
using System.Xml;
using System.Xml.Serialization;

//
// A class that contains Xml fields with data from players
public class PlayerData
{
    [XmlAttribute("name")]              public string Name;                 // The name of the player
    [XmlAttribute("score")]             public int Score;                   // The endscore of the player
    [XmlAttribute("date")]              public DateTime Date;               // The current date and time

    [XmlAttribute("levelTime")]         public int LevelTime;               // The gametimemode
    [XmlAttribute("duration")]          public float Duration;              // 
    [XmlAttribute("parachuteDuration")] public float ParachuteDuration;     // 
    [XmlAttribute("parachutesOpened")]  public int ParachutesOpened;        // 
    [XmlAttribute("freefallDuration")]  public float FreefallDuration;      // 

    [XmlAttribute("blueRings")]         public int BlueRingsHit;            // 
    [XmlAttribute("redRings")]          public int RedRingsHit;             // 
    [XmlAttribute("blackRings")]        public int BlackRingsHit;           // 

    [XmlAttribute("lowBuildings")]      public int LowBuildingsHit;         // 
    [XmlAttribute("mediumBuildings")]   public int MediumBuildingsHit;      // 
    [XmlAttribute("highBuildings")]     public int HighBuildingsHit;        // 
    [XmlAttribute("extremeBuildings")]  public int ExtremeBuildingsHit;     // 
}
