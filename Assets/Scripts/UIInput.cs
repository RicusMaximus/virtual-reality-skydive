﻿public class UIInput : UIItem
{


    private UnityEngine.UI.InputField m_input;


    protected override void HandleClick()
    {
        m_input.ActivateInputField();
        m_input.GetComponent<UnityEngine.UI.Image>().color = new UnityEngine.Color(255, 255, 255); // Reset input color
    }


    protected override void HandleOver() { } // Do nothing if the player hovers over the input field


    private void Start()
    {
        m_input = gameObject.GetComponent<UnityEngine.UI.InputField>();
    }
}