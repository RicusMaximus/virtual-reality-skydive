﻿using UnityEngine;
using UnityStandardAssets.CrossPlatformInput;

[RequireComponent(typeof(PlayerController))]
public class PlayerInput : MonoBehaviour
{
    // reference to the player that we're controlling
    private PlayerController m_player;
    private float m_forward;
    private float m_strafe;
    private float m_turn;
    private float m_bank;
    private bool m_jump;
    private bool m_run;
	private bool mm_togglePara;
    private float m_left;
    private float m_right;
    private bool m_toggle;
    public string[] Controllers;

    #region Getters & Setters
    PlayerController Player { get { return m_player; } set { m_player = value; } }

    public float Forward { get { return m_forward; } private set { m_forward = Mathf.Clamp(value, -1, 1); } }

    public float Strafe { get { return m_strafe; } private set { m_strafe = Mathf.Clamp(value, -1, 1); } }

    public float Turn { get { return m_turn; } private set { m_turn = Mathf.Clamp(value, -1, 1); } }

    public float Bank { get { return m_bank; } private set { m_bank = Mathf.Clamp(value, -1, 1); } }

    public float Left { get { return m_left; } private set { m_left = Mathf.Clamp(value, -1, 1); } }

    public float Right { get { return m_right; } private set { m_right = Mathf.Clamp(value, -1, 1); } }

    public bool Jump { get { return m_jump; } private set { m_jump = value; } }

    public bool Run { get { return m_run; } private set { m_run = value; } }

    public bool TogglePara { get { return mm_togglePara; } private set { mm_togglePara = value; } }

    public bool Toggle { get { return m_toggle; } private set { m_toggle = value; } }
    #endregion

    private void Awake ()
    {
        m_player = GetComponent<PlayerController>();
    }
        
    private void LateUpdate()
    {
        GetInput();
        m_player.Move(Forward, Strafe, Turn, Bank, Jump, Run, TogglePara, Left, Right, Toggle);
    }

    private void GetInput()
    {
        if (Input.GetJoystickNames().Length > 0) // Check whether there are joysticks connected (namely a Xbox controller)
        {
            Forward = CrossPlatformInputManager.GetAxis("Vertical");
            Strafe = CrossPlatformInputManager.GetAxis("Horizontal");
            Turn = CrossPlatformInputManager.GetAxis("RightJoystickX");
            Jump = CrossPlatformInputManager.GetButton("Jump");
            Run = CrossPlatformInputManager.GetButton("LeftStickClick");
            TogglePara = CrossPlatformInputManager.GetButtonDown("Jump");
            Left = CrossPlatformInputManager.GetAxis("LeftTrigger");
            Right = CrossPlatformInputManager.GetAxis("RightTrigger");
            Toggle = CrossPlatformInputManager.GetButtonDown("Toggle");
        }
        else // Keyboard input
        {
            Forward = CrossPlatformInputManager.GetAxis("Vertical");
            Strafe = CrossPlatformInputManager.GetAxis("Horizontal");
            Jump = CrossPlatformInputManager.GetButtonDown("Jump");
            Run = CrossPlatformInputManager.GetButton("Run");
            TogglePara = CrossPlatformInputManager.GetButtonDown("Open");
            Left = CrossPlatformInputManager.GetAxis("Left");
            Right = CrossPlatformInputManager.GetAxis("Right");
            Toggle = CrossPlatformInputManager.GetButtonDown("Toggle");
        }
        if (CrossPlatformInputManager.GetButtonDown("Open"))
            print("true");
    }
}