﻿using UnityEngine;
using System;
using System.Collections.Generic;
using UnityEngine.UI;

public class PfdController : MonoBehaviour {

    // Variables
    [SerializeField] private List<Text> _pitchText;
    [SerializeField] private Transform _cameraRotation;
    private float _speed = 0.75f;
    private bool _facefront = true;
    private Transform _player;
    private PlayerController _playerControl;
    public bool Grounded = false;

    public GameObject PitchCanvas, ScoreCanvas, HeightCanvas, DirectionCanvas, Near, Far, WindRig;

    // Methods
    void Awake()
    {
        _player = GameObject.Find("Player").transform;
        _playerControl = _player.gameObject.GetComponent<PlayerController>();
        var t = GetComponentsInChildren<Transform>();
        for (var i = 0; i < t.Length; i++)
        {
            if (t[i].tag == "PFDPitch")
                _pitchText.Add(t[i].GetComponent<Text>());
        }
    }

    void Start()
    {
        for (var i = 0; i < _pitchText.Count; i++)
        {
            var Xrot = Mathf.RoundToInt(_pitchText[i].gameObject.transform.parent.parent.rotation.eulerAngles.x);
            if (Xrot >= 180)
                Xrot -= 360;
            _pitchText[i].text = Xrot + "°";
        }
    }

    void Update ()
    {
        if (!Grounded && _playerControl.Rig.velocity.magnitude > 0.1f)
        {
            DirectionCanvas.SetActive(true);
            SetCanvasRotation(Far, _speed);
            SetCanvasRotation(Near, _speed / 3);
            SetCanvasRotation(WindRig, 1f);
        }
        else
            DirectionCanvas.SetActive(false);
        transform.rotation = Quaternion.Euler(0, _player.transform.localEulerAngles.y, 0);

        var rot = _cameraRotation.localRotation;
        var back = transform.forward;
        if (Quaternion.Angle(rot, Quaternion.Euler(back)) > 140)
        {
            if (_facefront)
            {
                foreach (Text text in _pitchText)
                {
                    text.transform.localRotation = Quaternion.Euler(new Vector3(0, 0, 180));
                    text.GetComponent<RectTransform>().anchoredPosition *= -1; 
                }
            }
            _facefront = false;
        }

        else
        {
            if (!_facefront)
            {
                foreach (Text text in _pitchText)
                {
                    text.transform.localRotation = Quaternion.Euler(new Vector3(0, 0, 0));
                    text.GetComponent<RectTransform>().anchoredPosition *= -1;
                }
            }
            _facefront = true;
        }
    }

    public void ToggleDisplay(int display)
    {
        // Turn off all display types
        PitchCanvas.SetActive(false);
        ScoreCanvas.SetActive(false);
        HeightCanvas.SetActive(false);

        switch (display)
        {
            // Display 1: All GUI
            case 0:
                PitchCanvas.SetActive(true);
                ScoreCanvas.SetActive(true);
                HeightCanvas.SetActive(true);
                break;

            // Display 2: Just Height and Score
            case 1:
                ScoreCanvas.SetActive(true);
                HeightCanvas.SetActive(true);
                break;

            // Display 3: Just the Pitch and Turn
            case 2:
                PitchCanvas.SetActive(true);
                break;

            // Display 4: None
            case 3:
                break;
            default:
                throw new InvalidOperationException();// Should not be reachable

        }
    }
    private void SetCanvasRotation(GameObject obj, float speed)
    {
        obj.gameObject.transform.rotation = Quaternion.Slerp(obj.gameObject.transform.rotation, Quaternion.LookRotation(_playerControl.Rig.velocity, _player.up), speed);
    }
}
