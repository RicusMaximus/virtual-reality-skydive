﻿// CoroutineUtil.cs
using UnityEngine;
using System.Collections;


/// <summary>
/// This class is a utility class for Coroutines
/// </summary>
[AddComponentMenu("Utility/Realtime Coroutine")]
public static class CoroutineUtil
{
    /// <summary>
    /// Waits for real seconds.
    /// </summary>
    /// <remarks>
    /// This utility is used for Coroutines which have to run outside the game timescale.
    /// </remarks>
    /// <param name="time">The time.</param>
    /// <returns>Returns the amount of time passed in realtime seconds.</returns>
    public static IEnumerator WaitForRealSeconds(float time)
    {
        float start = Time.realtimeSinceStartup;
        while (Time.realtimeSinceStartup < start + time)
        {
            yield return null;
        }
    }
}
