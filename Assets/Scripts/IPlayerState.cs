﻿public interface IPlayerState
{
    void OnTransitionComplete(UnityEngine.Animator animator, UnityEngine.AnimatorStateInfo stateInfo, int layerIndex);
}