﻿// GameManager.cs
using UnityEngine;


/// <summary>
/// Game States
/// </summary>
public enum GameState
{
    /// <summary>The Intro game state.</summary>
    INTRO,
    /// <summary>The Main Menu game state.</summary>
    MAIN_MENU,
    /// <summary>The Paused game state.</summary>
    PAUSED,
    /// <summary>The Tutorial game state.</summary>
    TUTORIAL,
    /// <summary>The InGame game state.</summary>
    GAME,
    /// <summary>The Credits game state.</summary>
    CREDITS,
    /// <summary>The Help game state.</summary>
    HELP,
    /// <summary>The Options game state.</summary>
    OPTIONS,
    /// <summary>The GameOver game state.</summary>
    GAMEOVER
}

/// <summary>
/// A delegate function for state changes.
/// </summary>
public delegate void OnStateChangeHandler();

/// <summary>
/// The Game Manager adheres to the Singleton design pattern.
/// </summary>
/// <remarks>
/// The Game Manager handles the Game States 
/// </remarks>
public class GameManager : MonoBehaviour
{

    /// <summary>
    /// Occurs when the game state changes.
    /// </summary>
    public event OnStateChangeHandler OnStateChange;

    /// <summary>
    /// Gets the current state of the game.
    /// </summary>
    /// <value>
    /// The state of the game.
    /// </value>
    public GameState gameState { get { return m_gameState; } private set { value = m_gameState; } }

    [SerializeField] private GameState m_gameState;

    /// <summary>
    /// The manager instance.
    /// </summary>
    private static GameManager manager = null;

    /// <summary>
    /// Gets the manager instance.
    /// </summary>
    /// <value>
    /// The manager instance.
    /// </value>
    public static GameManager Manager { get { return manager; } }

    public bool Loaded = false;

    /// <summary>
    /// Initialized the class in runtime.
    /// </summary>
    void Awake()
    {
        GetGameManager();
    }

    /// <summary>
    /// Prints the current game state.
    /// </summary>

    /// <summary>
    /// Sets the state of the game.
    /// </summary>
    /// <param name="state">The game state.</param>
    public void SetGameState(GameState state){
		m_gameState = state;
		OnStateChange();
	}

    /// <summary>
    /// Called when the application quits.
    /// </summary>
    public void OnApplicationQuit(){ manager = null; }

    /// <summary>
    /// Gets the game manager or creates one.
    /// </summary>
    void GetGameManager()
    {
        if (manager != null && manager != this)
        {
            Destroy(gameObject);
            return;
        }
        else
            manager = this;
        DontDestroyOnLoad(gameObject); // Keep the object intact between loading scenes

        Loaded = false;
    }
}
