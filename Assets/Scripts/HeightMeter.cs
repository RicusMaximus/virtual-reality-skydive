﻿using UnityEngine;

public class HeightMeter : MonoBehaviour {

    private UnityEngine.UI.Text m_heightText;
    private Transform m_player;

	// Use this for initialization
	void Start () {
        m_player = GameObject.Find("Player").GetComponent<Transform>();
        m_heightText = GetComponent<UnityEngine.UI.Text>();
	}
	
	// Update is called once per frame
	void Update () {
        // Set HUD text heightmeter
        m_heightText.text = "Hoogte:\n" + Mathf.Round(m_player.position.y) + "m";
    }
}
