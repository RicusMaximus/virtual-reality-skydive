﻿using UnityEngine;

public class StartTrigger : MonoBehaviour {

	private System.Collections.IEnumerator m_start;    // Enumerator for lowering the bonus value
	private bool m_started;

	// Use this for initialization
	void Start ()
	{
		m_start = GameObject.Find("bonusText").GetComponent<Bonus>().DecreaseBonus();				// Set the enumerator to the Decrease Bonus function in the bonusscript
		m_started = false;
	}
	
	// Update is called once per frame
	void OnTriggerEnter (Collider other)
	{
		if (other.name != "Player") return;				// Only execute the function for the player
		if (m_started) return;							// Execute once
		StartCoroutine(m_start);
		m_started = true;
	}
}
