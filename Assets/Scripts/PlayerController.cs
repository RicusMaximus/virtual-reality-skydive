﻿using UnityEngine;
using UnityEngine.Audio;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;
using System;

/*
 * Rico Robinson, 23-10-2016
 * Script to control player object calculations for the skydive project
 * Gets input from PlayerInput script and is connected to a Unity Mecanim State Machine with different State Machine Behaviours
 *  
 * */
 
public enum HitDirection { None, Top, Bottom, Forward, Back, Left, Right }

[RequireComponent(typeof(Rigidbody))]
[RequireComponent(typeof(Animator))]
[RequireComponent(typeof(CapsuleCollider))]
public class PlayerController : MonoBehaviour
{

    // Private properties
    private float _altitude, _forward, _strafe, _turn, _bank, _left, _right, _originalDrag, _boundForceFactor = 1f, _score = 0, _levelFactor = 1.8f, _counter = 0f;
    private bool _jump, _run, _togglePara, m_FfToggled, _detach, _toggleDisplay, _addedScore, _warning, _toggled;
    private int _portalsHit = 0, _dispNr = 0;
    private UnityStandardAssets.ImageEffects.TiltShift _tiltShift;
    private PlayerInput _playerInput;
    private Rigidbody _rigidbody;
    private CapsuleCollider _collider;
    private RaycastHit _hit;
    private GameObject ObjectHit;
    private Text _distText;
    private GameMaster _master;
    private IEnumerator m_parachuteDelay;

    // Serialized in UnityEditor properties
    [SerializeField]
    private Parachute _parachute;
    [SerializeField]
    private GameObject _distCanvas, _logoCanvas, _windCanvas;
    [SerializeField]
    private Text _scoreText, _flashText, _bonusText;
    [SerializeField]
    private Camera _mainCamera, _uiCam, _closeCam;
    [SerializeField]
    private AudioClip _musicClip, _windClip, _pointsClip, _parachuteFlapClip, _parachuteOpenClip, _toggleDisplayClip;
    [SerializeField]
    private ParticleSystem _windEffect;
    [SerializeField]
    private Bonus _bonus;

    // Public properties
    public PlayerData PlayerData;
    public GameObject Para, Logo;
    public CanvasGroup FlashScore, Warning;
    public PfdController PFD;
    public List<GameObject> _scoredObjs;
    GameManager GM;

    // Audio sources
    [HideInInspector] public AudioSource _audioCenterSrc, _musicSrc, _windSrc, _parachuteFlapSrc;

    // Audio Mixer Groups
    public AudioMixerGroup _masterGroup, _musicGroup, _SFXGroup;

    // Encapsulated properties
    public float Altitude
    {
        get { return _altitude; }
        private set { _altitude = value; }
    }
    public float OriginalDrag
    {
        get { return _originalDrag; }
        private set { _originalDrag = value; }
    }
    public CapsuleCollider Col
    {
        get { return _collider; }
    }
    public Camera Cam
    {
        get { return _mainCamera; }
        private set
        {
            if (value is Camera) _mainCamera = value;
            else throw new ArgumentException(); return;
        }
    }
    public Rigidbody Rig
    {
        get { return _rigidbody; }
        private set
        {
            if (value is Rigidbody) _rigidbody = value;
            else throw new ArgumentException(); return;
        }
    }
    public ParticleSystem Wind
    {
        get { return _windEffect; }
        private set { _windEffect = value; }
    }

    #region Input
    // Input
    public float Forward
    {
        get { return _forward; }
        private set { _forward = value; }
    }
    public float Strafe
    {
        get { return _strafe; }
        private set { _strafe = value; }
    }
    public float Turn
    {
        get { return _turn; }
        private set { _turn = value; }
    }
    public float Bank
    {
        get { return _bank; }
        private set { _bank = value; }
    }
    public bool Jump
    {
        get { return _jump; }
        private set { _jump = value; }
    }
    public bool Run
    {
        get { return _run; }
        private set { _run = value; }
    }
    public bool TogglePara
    {
        get { return _togglePara; }
        private set { _togglePara = value; }
    }
    public bool FfToggled
    {
        get { return m_FfToggled; }
        set { m_FfToggled = value; }
    }
    public bool ToggleDisplay
    {
        get { return _toggleDisplay; }
        private set { _toggleDisplay = value; }
    }
    public bool Detach
    {
        get { return _detach; }
        private set { _detach = value; }
    }
    public float Left
    {
        get { return _left; }
        private set { _left = value; }
    }
    public float Right
    {
        get { return _right; }
        private set { _right = value; }
    }
    #endregion

    public void ResetDrag() { Rig.drag = 0; }

    /// <summary>
    /// Initialisation of references that can only be set during runtime
    /// </summary>
    private void Awake()
    {
        Para = Instantiate(_parachute.gameObject, transform.position, transform.rotation) as GameObject;                    // Create Parachute instance as an independent GameObject
        Para.transform.SetParent(gameObject.transform);                                                                     // Set parachute clone as child of player
        Para.transform.position += new Vector3(0f, 0.5f, 0f);
        Para.SetActive(false);
        GM = GameManager.Manager;
        PlayerData = new PlayerData();
        _collider = GetComponent<CapsuleCollider>();
        _playerInput = gameObject.AddComponent<PlayerInput>() as PlayerInput;
        _distText = _distCanvas.GetComponentInChildren<Text>();
        Rig = GetComponent<Rigidbody>();
        PFD = GetPFD();
        _tiltShift = transform.Find("CameraRigPosition/MainCam").GetComponent<UnityStandardAssets.ImageEffects.TiltShift>();
        _master = GameObject.Find("GameMaster").GetComponent<GameMaster>();
        OriginalDrag = Rig.drag;
        _scoreText.text = _score.ToString();
        _scoreText.color = new Color(1f, .5f, 0f);
        _flashText.color = new Color(1f, .5f, 0f);
        _bonus = GetComponentInChildren<Bonus>();
        _windCanvas = Wind.transform.parent.gameObject;

        m_parachuteDelay = DelayParachute();
        
        // set audio
        _musicSrc = GameMaster.AddAudio(Cam.gameObject, _musicClip, _musicGroup, true, true, 0.08f, 129);
        _windSrc = GameMaster.AddAudio(Cam.gameObject, _windClip, _SFXGroup, true, false, 0f, 128);
        _parachuteFlapSrc = GameMaster.AddAudio(Cam.gameObject, _parachuteFlapClip, _SFXGroup, true, false, 0.05f, 128);
        _audioCenterSrc = GameMaster.AddAudio(Cam.gameObject, null, _SFXGroup, false, false, 0.3f, 1);

        _musicSrc.Play();
        _windSrc.Play();
    }
    
    public PfdController GetPFD () {
        return PFD ?? GetComponentInChildren<PfdController>();
    }
    
    private void Update()
    {
        RaycastHit hit;
        Vector3 origin = Cam.transform.position;
        Vector3 direction = Cam.transform.forward;
        Ray ray = new Ray(origin, direction);
        if (Physics.Raycast(ray, out hit, 1000f))
            HitObject(hit);
        else
        {
            _distCanvas.gameObject.SetActive(false);
        }

        if (_addedScore)
        {
            FlashScore.alpha = FlashScore.alpha - Time.deltaTime;
            if (FlashScore.alpha <= 0)
            {
                FlashScore.alpha = 0;
                _addedScore = false;
            }
        }

        if (_warning)
        {
            
            Warning.alpha = Mathf.Abs(Mathf.Cos(_counter)); // The alpha value of the warning bounces between 0 and 1 until parachute is dropped
            _counter += Time.deltaTime;

            if (!Para.activeSelf)
            {
                Warning.alpha = 0;
                _warning = false;
                _counter = 0;
            }
        }

        // ToggleDisplay display on ToggleDisplay input
        if (ToggleDisplay)
        {
            _audioCenterSrc.PlayOneShot(_toggleDisplayClip);
            _dispNr++;
            PFD.ToggleDisplay(_dispNr % 4); // Toggle between 4 states
        }

        if (transform.position.y < 2f && GM.gameState == GameState.GAME)
        {
            GM.SetGameState(GameState.GAMEOVER);
            _score += _bonus.BonusValue;
            PlayerData.Score = Mathf.RoundToInt(_score);
            _master.PlayerData = PlayerData;
            _master.Invoke("GameOver", 2f);
        }
    }

    private void HitObject(RaycastHit hit)
    {
        if (GM.gameState != GameState.GAME)
        {
            _distCanvas.gameObject.SetActive(false);
            return; // Do not display when not playing
        }
        else if (!_distCanvas.activeSelf)
            _distCanvas.gameObject.SetActive(true);
        
        string name = hit.collider.name;
        Vector3 dist = (hit.point - transform.position);
        _distText.text = name + ": \n" + Mathf.Round(dist.magnitude * 10) / 10 + "m"; // Round for 1 decimal
    }

    //
    // FixedUpdate is called once per physics timestep (0.02 sec)
    private void FixedUpdate()
    {
        CalculateAltitude();
        KeepWithinBounds();
        ControlAudio();
        ControlSpeedEffect();
    }

    private void KeepWithinBounds()
    {
        Vector3 flatDist = transform.position;
        flatDist.y = 0;
        if (flatDist.magnitude > 2000f)
        {
            Vector3 boundaryForce = -flatDist.normalized * (flatDist.magnitude - 2000f) * _boundForceFactor;
            Rig.AddForce(boundaryForce, ForceMode.Force);
            print(boundaryForce);
        }
    }

    // Public methods
    #region Public Methods
    public bool AboveGround()
    {
        // Spherecast downwards to take width of character in account
        return Physics.SphereCast(transform.position, 0.5f, -Vector3.up, out _hit, 10.0f);
    }

    public void Move(float forwardInput, float strafeInput, float turnInput, float bankInput, bool jumpInput, bool runInput, bool toggleParaInput, float leftInput, float rightInput, bool toggleDisplayInput)
    {
        #region Assign inputs
        Forward = forwardInput;
        Strafe = strafeInput;
        Turn = turnInput;
        Bank = bankInput;
        Jump = jumpInput;
        Run = runInput;
        if (!_toggled) TogglePara = toggleParaInput; else TogglePara = false;
        Left = leftInput;
        Right = rightInput;
        ToggleDisplay = toggleDisplayInput;
        #endregion
    }


    public void OpenParachute()
    {
        _audioCenterSrc.PlayOneShot(_parachuteOpenClip, 0.1f);
        _parachuteFlapSrc.PlayDelayed(1.5f);
        Para.GetComponent<Parachute>().TogglePara = true;
        Para.SetActive(true);
    }

    private void ControlAudio()
    {
        _windSrc.volume = Mathf.Lerp(_windSrc.volume, (Mathf.Pow(Rig.velocity.magnitude / 200f, 2)), 0.01f);
    }


    public void DropParachute()
    {
        _parachuteFlapSrc.Stop();
        Para.GetComponent<Parachute>().TogglePara = false;
        Para.SetActive(false);
    }

    public IEnumerator WarnPlayer()
    {
        int timer = 0;
        while (timer < 59)
        {
            if (!Para.activeSelf)
            {
                StopCoroutine(WarnPlayer());
                break;
            }

            timer++;
            print(timer);
            yield return new WaitForSeconds(1f);
        }
        // Another check after break
        if (Para.activeSelf)
        {
            ////////////////////////////
            //    Activate warning    //
            ////////////////////////////
            Warning.alpha = 1f;
            _warning = true;
        }
    }
    #endregion

    // Private methods
    #region Private Methods
    private void CalculateAltitude()
    {
        // Altitude calculations - we raycast downwards from the player
        // Only intersect any objects which are in the terrain layer
        var ray = new Ray(transform.position, -Vector3.up);
        Physics.Raycast(ray, out _hit, 1 << 8);

        if (_hit.collider != null && !_hit.collider.isTrigger)
            Altitude = _hit.distance - 0.95f;
        else
            Altitude = transform.position.y;
    }

    void OnCollisionEnter(Collision other)
    {
        if (ReturnDirection(other.gameObject, gameObject) != HitDirection.Top)
        {
            RaycastHit hit;
            Physics.Raycast(transform.position, Rig.velocity, out hit, 10000f);
            Rig.AddForceAtPosition(hit.normal * 100f + hit.normal * Rig.velocity.magnitude, hit.point, ForceMode.Impulse);
        }

        else if (other.gameObject.tag == "Points")
        {
            {
                PlaceImage(other);
                float points = other.gameObject.GetComponent<Building>().Points;
                AddPoints(points, other.gameObject);
            }
        }
    }

    void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.tag == "Portal")
        {
            // Get value from script
            int points = other.GetComponentInParent<Portalscript>().Points;

            // Chain Bonus
            points += 1000 * _portalsHit;
            _portalsHit += 1;
            AddPoints(points, other.gameObject);
        }
    }

    private void PlaceImage(Collision other)
    {
        Vector3 offset = Vector3.zero;
        offset.y += 0.025f; // y-offset for z-fighting fix

        Logo = Instantiate(_logoCanvas, other.contacts[0].point + offset, Quaternion.Euler(new Vector3(90f, transform.localEulerAngles.y, 0f))) as GameObject; // Place Wowzone logo
        var rect = Logo.GetComponentsInChildren<RectTransform>();
        var flatSpeed = Rig.velocity;
        flatSpeed.y = 0;
        rect[1].localPosition += new Vector3(0f, flatSpeed.magnitude / 2f, 0f);
    }

    private void AddPoints(float points, GameObject obj)
    {
        // Align FlashScore canvas to playerview
        FlashScore.transform.rotation = _closeCam.transform.rotation;

        if (!_scoredObjs.Contains(obj)) // You can only gain points once per session per points object (building / portal)
        {
            // Play points audio
            _audioCenterSrc.PlayOneShot(_pointsClip, 0.4f);

            // Flash points in screen
            _addedScore = true;
            _flashText.text = "+" + points;
            FlashScore.alpha = 1;

            // Add value to total
            _score += points;
            _scoreText.text = _score.ToString();

            _scoredObjs.Add(obj);

            print(_score);
        }
    }

    public IEnumerator DelayParachute()
    {
        _toggled = true;
        yield return new WaitForSeconds(2);
        _toggled = false;
    }

    public IEnumerator DelayFreefall()
    {
        m_FfToggled = true;
        yield return new WaitForSeconds(2.0f);
        m_FfToggled = false;
    }

    private void ControlSpeedEffect()
    {
        float speed = Rig.velocity.magnitude;
        
        // Tilt Shift blur effect
        _tiltShift.maxBlurSize = speed / 10f;
        //print("Speed: " + speed + " | Max blur size: " + _tiltShift.maxBlurSize);
        
        // Wind particle effect
        float rotFactor = Mathf.Clamp(1 - Vector3.Angle(_windCanvas.transform.forward, Cam.transform.forward) / 90, 0f, 1f);
        Wind.startSpeed = speed;
        Wind.emissionRate = speed * 3;
        Wind.startColor = new Color(speed * 5 / 255f, speed * 5 / 255f, speed * 5 / 255f, rotFactor / 3.5f);
    }

    public void ResetRotation()
    {
        Vector3 rot = transform.rotation.eulerAngles;
        rot.x = 0;
        rot.z = 0;
        transform.rotation = Quaternion.Euler(rot);
    }

    public HitDirection ReturnDirection(GameObject Object, GameObject ObjectHit)
    {

        HitDirection hitDirection = HitDirection.None;
        RaycastHit MyRayHit;
        Vector3 direction = (Object.transform.position - ObjectHit.transform.position).normalized;
        Ray MyRay = new Ray(ObjectHit.transform.position, direction);

        if (Physics.Raycast(MyRay, out MyRayHit))
        {

            if (MyRayHit.collider != null)
            {

                Vector3 MyNormal = MyRayHit.normal;
                MyNormal = MyRayHit.transform.TransformDirection(MyNormal);

                if (MyNormal == MyRayHit.transform.up) { hitDirection = HitDirection.Top; }
                if (MyNormal == -MyRayHit.transform.up) { hitDirection = HitDirection.Bottom; }
                if (MyNormal == MyRayHit.transform.forward) { hitDirection = HitDirection.Forward; }
                if (MyNormal == -MyRayHit.transform.forward) { hitDirection = HitDirection.Back; }
                if (MyNormal == MyRayHit.transform.right) { hitDirection = HitDirection.Right; }
                if (MyNormal == -MyRayHit.transform.right) { hitDirection = HitDirection.Left; }
            }
        }
        return hitDirection;

    }
    #endregion
}