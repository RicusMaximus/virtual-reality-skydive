﻿using UnityEngine;
using System.Collections;

public class PortalLODScript : MonoBehaviour {

    private GameObject _spire;
    private Portalscript _portal;
    private int _direction = 1;
    
    [SerializeField] private float _speed = 1f;

    void Awake ()
    {
        _spire = GameObject.Find("Center Spire");
        _portal = GetComponentInChildren<Portalscript>();
        if ((int)transform.position.y % 2 == 0)
            _direction *= -1;

    }
    
    void Start ()
    {
        var value = _spire.transform.position - transform.position;
        Vector3.ProjectOnPlane(value, Vector3.up);
        _portal.Points += 1000 * (int)(Mathf.Round(value.magnitude) / 100);
    }

    void Update ()
    {
        transform.RotateAround(_spire.transform.position, Vector3.up * _direction, _speed * Time.deltaTime);
    }

}
