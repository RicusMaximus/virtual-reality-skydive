﻿public class UIHighscore : UIItem {

    protected override void HandleClick()
    {
        m_highscoreManager.OnVRClick(m_button);
    }

    protected override void HandleOver()
    {
        base.HandleOver();
    }
}
