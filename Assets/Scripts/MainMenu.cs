﻿using UnityEngine;

public class MainMenu : MonoBehaviour
{

    GameManager GM;
    GameMaster _master;

    // Audio Clips
    [SerializeField] private AudioClip _musicClip, _hoverClip, _clickClip;

    // Audio sources
    private AudioSource _audioCenterSrc, _musicSrc;

    // Audio Mixer Groups
    public UnityEngine.Audio.AudioMixerGroup _masterGroup, _musicGroup, _SFXGroup;

    void Awake () {
        _master = GameObject.Find("GameMaster").GetComponent<GameMaster>();
        _musicSrc = GameMaster.AddAudio(gameObject, _musicClip, _musicGroup, true, true, 0.1f, 100);
        _audioCenterSrc = GameMaster.AddAudio(gameObject, null, _SFXGroup, false, false, 1f, 1);
        _musicSrc.Play();
    }

	public void HandleOnStateChange ()
	{
		Debug.Log("OnStateChange!");
	}

    public void OnVRHover()
    {
        _audioCenterSrc.PlayOneShot(_hoverClip, 0.3f);
    }

    public void OnVRClick(string button)
    {
        switch (button)
        {
            case "Start":
                _audioCenterSrc.PlayOneShot(_clickClip, 0.5f);
                _master.StartGame();
                break;
            case "Highscores":
                break;
            case "Tutorial":
                _master.StartTutorial();
                break;
            case "Options":
                ShowOptions();
                break;
            case "Credits":
                ShowCredits();
                break;
            case "Exit":
                _audioCenterSrc.PlayOneShot(_clickClip, 0.5f);
                Quit();
                break;
        }
    }

    public void ShowOptions()
    {
        GM.SetGameState(GameState.OPTIONS);
        // SLIDE IN OPTIONS PANEL
        Debug.Log(GM.gameState);
    }

	public void ShowHelp(){
		// show Help scene or GUI
		GM.SetGameState(GameState.HELP);
        // GIVE INFO ON HOW TO PLAY GAME
		Debug.Log(GM.gameState);
	}

    public void ShowCredits()
    {
        // show credits scene
        GM.SetGameState(GameState.CREDITS);
        Debug.Log(GM.gameState);
    }

    public void Quit(){
		Debug.Log("Quit!");
		Application.Quit();
	}
}
