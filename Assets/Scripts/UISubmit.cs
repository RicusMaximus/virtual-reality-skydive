﻿using UnityEngine;

public class UISubmit : UIItem
{

    [SerializeField]
    private UnityEngine.UI.InputField m_input;


    protected override void HandleOver()
    {
        base.HandleOver();
        m_gameMaster.OnVRHover();
    }


    //Handle the Click event
    protected override void HandleClick()
    {
        if (!m_gameMaster.OnNameSubmit(m_input.text))
        {
            print("OnNameSubmit failed");
            m_input.GetComponent<UnityEngine.UI.Image>().color = new Color(255, 200, 200); // Give input field a red tone
        }
    }

}
